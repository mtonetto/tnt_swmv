﻿using GalaSoft.MvvmLight;

namespace Tnt.SWMV.ViewModels
{
    /// <summary>
    /// Represents an input VM.
    /// </summary>
    public class InputViewModel : ViewModelBase
    {
        #region Fields

        /// <summary>
        /// Represents the inputed text.
        /// </summary>
        private string _text = string.Empty;

        /// <summary>
        /// Represents the length of the text.
        /// </summary>
        private int _count = 0;

        #endregion

        #region Constructors

        // TODO : MT put constructors here.

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the inputed text.
        /// </summary>
        public string Text
        {
            get => _text;
            set
            {
                if (Set(() => Text, ref _text, value))
                {
                    OnTextChanged();
                }
            }
        }

        /// <summary>
        /// Gets the number of inputed chars.
        /// </summary>
        public int Count
        {
            get => _count;
            private set => Set(() => Count, ref _count, value);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Calls when Text property changed.
        /// </summary>
        private void OnTextChanged()
        {
            Count = Text.Length;
        }

        #endregion
    }
}
