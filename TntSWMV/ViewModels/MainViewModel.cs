using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using System.Collections.Generic;

namespace Tnt.SWMV.ViewModels
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        #region Fields

        private ViewModelBase _currentViewModel;

        /// <summary>
        /// Represents the index in the VM list.
        /// </summary>
        private int _index = 0;

        /// <summary>
        /// Represents the static instance of the cockpit VM.
        /// </summary>
        private readonly static List<ViewModelBase> _viewModels = new List<ViewModelBase>();

        #endregion

        #region Constructors

        /// <summary>
        /// Static constructor.
        /// </summary>
        static MainViewModel()
        {
            _viewModels.Add(new CockpitViewModel());

            _viewModels.Add(new SettingsViewModel());

            _viewModels.Add(new InputViewModel());

            _viewModels.Add(new AboutViewModel());
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MainViewModel"/> class.
        /// </summary>
        public MainViewModel()
        {
            ////if (IsInDesignMode)
            ////{
            ////    // Code runs in Blend --> create design time data.
            ////}
            ////else
            ////{
            ////    // Code runs "for real"
            ////}
            ////

            CurrentViewModel = _viewModels[Index];

            PrevCommand = new RelayCommand(PrevExecute);

            NextCommand = new RelayCommand(NextExecute);
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the current index of the VM list.
        /// </summary>
        private int Index
        {
            get => _index;
            set
            {
                if (Set(() => Index, ref _index, value))
                {
                    OnIndexChanged();
                }
            }
        }

        /// <summary>
        /// Gets the current VM.
        /// </summary>
        public ViewModelBase CurrentViewModel
        {
            get => _currentViewModel;
            private set => Set(() => CurrentViewModel, ref _currentViewModel, value);
        }

        #region Properties - Commands

        /// <summary>
        /// Gets the prev command.
        /// </summary>
        public RelayCommand PrevCommand { get; private set; }

        /// <summary>
        /// Gets the next command.
        /// </summary>
        public RelayCommand NextCommand { get; private set; }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Calls when index property change.
        /// </summary>
        private void OnIndexChanged()
        {
            CurrentViewModel = _viewModels[Index];
        }

        /// <summary>
        /// Prev command execution
        /// </summary>
        private void PrevExecute()
        {
            var index = Index;
            if (--index < 0)
            {
                index = _viewModels.Count - 1;
            }
            Index = index;
        }

        /// <summary>
        /// Next command execution.
        /// </summary>
        private void NextExecute()
        {
            var index = Index;
            if (++index >= _viewModels.Count)
            {
                index = 0;
            }
            Index = index;
        }

        #endregion
    }
}