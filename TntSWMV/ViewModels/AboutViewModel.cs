﻿using GalaSoft.MvvmLight;
using System.Linq;

namespace Tnt.SWMV.ViewModels
{
    /// <summary>
    /// Represents the about VM.
    /// </summary>
    public class AboutViewModel : ViewModelBase
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="AboutViewModel"/> class.
        /// </summary>
        public AboutViewModel()
        {
            System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();

            Version = assembly.GetName().Version.ToString();
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the assembly version.
        /// </summary>
        public string Version { get; }

        #endregion
    }
}
