﻿using System.Windows;

namespace Tnt.SWMV
{
    /// <summary>
    /// Represents the code behind for MainWindow.xaml.
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Constructors

        public MainWindow()
        {
            InitializeComponent();
        }

        #endregion
    }
}