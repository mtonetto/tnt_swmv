﻿namespace Tnt.SWMV.Models
{
    /// <summary>
    /// Represents an attribute.
    /// </summary>
    /// <typeparam name="T">The type of the abbribute.</typeparam>
    public abstract class Attribute<T>
    {
        #region Properties

        /// <summary>
        /// Gets the name.
        /// </summary>
        public string Name;

        /// <summary>
        /// Gets the value.
        /// </summary>
        public T Value;

        #endregion

        #region Methods



        #endregion
    }
}
