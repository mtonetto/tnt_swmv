﻿namespace Tnt.SWMV.Models
{
    /// <summary>
    /// Represents a parameter.
    /// </summary>
    /// <typeparam name="Decimal"></typeparam>
    public class Parameter : Attribute<decimal>
    {
    }
}
