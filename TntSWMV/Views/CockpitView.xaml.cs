﻿using System.Windows.Controls;

namespace Tnt.SWMV.Views
{
    /// <summary>
    /// Represents the code behind for CockpitView.xaml.
    /// </summary>
    public partial class CockpitView : UserControl
    {
        #region Constructors

        public CockpitView()
        {
            InitializeComponent();
        }

        #endregion
    }
}
